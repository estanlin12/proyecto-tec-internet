document.querySelector('.btn-responsive').addEventListener('click', () => {
	document.querySelector('.nav-menu').classList.toggle('show');
});

ScrollReveal().reveal('.showcase');
ScrollReveal().reveal('.newscards', { delay: 500 });
ScrollReveal().reveal('.section-cards-banner1', { delay: 500 });
ScrollReveal().reveal('.section-cards-banner2', { delay: 500 });

