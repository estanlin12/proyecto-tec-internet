# Proyecto-Tec-Internet

Saludos, aqui se estaran suabiendo los archivos del proyecto con la documentación segun lo requerido ya hablado en la ultima clase del jueves 7 de abril del año 2022. Nuestro  proyecto es llamado Tecnoticias. 

---------------------------------------------------------------------------------------------------------------------------------

                                     PROYECTO FINAL - TECNOLOGIA Y SERVICIOS DE INTERNET

#INTEGRANTES: JONATHAN VILLEGAS Y ESTARLIN NUÑEZ

#DE QUE TRATA EL PROYECTO:

Este proyecto consiste en una pagina web de noticias en cuanto al area de tecnologia. En esta pagina se publicara toda noticia en cuanto a tecnología en lo que es la Rep.Dom, sus avances en tecnologia, desarrollo, etc... 
Sera a nivel mundial pero hará mas enfoque en lo que son las noticias tecnologicas en nuestro país. 

#A QUE PUBLICO VA DIRIGIDO:

Va dirigida a todo publico interesado en lo que es la tecnologia.

#QUE TECNOLOGIAS ESTAMOS UTILIZANDO:

Para el desarrollo de esta pagina estamos utilizando HTML5 CSS3 y muy poco de JavaScript ya que lo dominamos muy poco. Editores de texto: SublimeText y Visual Studio Code.

#LAS PARTES QUE LE TOCA A CADA QUIEN:

JONATHAN VILLEGAS:

Se esta encargado de desarrollar el index.html(pagina de inicio) y los tres ultimos temas del menu. Para el desarrollo a utilizado las mismas tecnologias ya mencionadas, html, css y javascript. 

ESTARLIN NUÑEZ:

Se a encargado de manejar y organizar todo en cuanto Git ya que lo maneja mejor que Jonathan V. Se encargado de organizar los enlaces en cuanto al archivo index.html y tambien va desarrollando los tres primeros temas del menu.
